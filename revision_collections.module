<?php
/**
* @file
* function implementations of the revision collections module
*/

/**
 * Implements hook_menu()
 *
 */
function revision_collections_menu() {
	$items['admin/config/content/revision_collection'] = array(
		'title' => 'Revision collection',
		'page callback' => 'drupal_get_form',		
		'page arguments' => array('revision_collections_settings_form'),		
		'access arguments' => array('administer revision collection'),
		'type' => MENU_LOCAL_TASK,
		'file' => 'revision_collections.admin.inc',
	);
	$items['admin/content/revision_collection'] = array(
		'title' => 'Revision collections',
		'page callback' => 'revision_collections_overview',		
		'access arguments' => array('create revision collections'),
		'type' => MENU_LOCAL_TASK,
		'file' => 'revision_collections.admin.inc',		
	);
	$items['admin/content/revision_collection/add'] = array(
		'title' => 'Add revision collections',
		'page callback' => 'revision_collections_add',
		'access arguments' => array('create revision collections'),
		'type' => MENU_NORMAL_ITEM,	
		'file' => 'revision_collections.admin.inc',			
	);
	$items['content/revision_collection/%/edit'] = array(
		'title' => 'Edit revision collection',
		'page callback' => 'revision_collections_edit',
		'page arguments' => array(2),
		'access arguments' => array('edit revision collections'),
		'type' => MENU_CALLBACK,	
		'file' => 'revision_collections.admin.inc',		
	);
	$items['content/revision_collection/%/delete'] = array(
		'title' => 'Add revision collections',		
		'page callback' => 'drupal_get_form',
		'page arguments' => array('revision_collection_delete_confirm', 2),
		'access arguments' => array('delete revision collections'),
		'type' => MENU_CALLBACK,	
	'file' => 'revision_collections.admin.inc',			
	);
	return $items;
}
/**
 * Implements hook_permission()
 *
 */
function revision_collections_permission() {
  return array(
    'administer revision collection' => array(
      'title' => t('Administer revision collection'),
      'description' => t('Perform administration tasks for revision collection module.'),
    ),
    'create revision collections' => array(
      'title' => t('Create revision collections'),
      'description' => t('The permission to create revision collections'),
    ),
    'edit revision collections' => array(
      'title' => t('Edit a revision collections'),
      'description' => t('The permission to edit a revision collections'),
    ),
    'delete revision collections' => array(
      'title' => t('Delete revision collections'),
      'description' => t('The permission to delete revision collections'),
    ),
  );
}

/**
 * Implements of hook_admin_paths()
 *
 */
function revision_collections_admin_paths(){
	$paths = array(
    'content/revision_collection/*/delete' => TRUE,
    'content/revision_collection/*/edit' => TRUE,
  );
  return $paths;
}
/**
 * Deletes a collection
 *
 * @param $cid
 *   The id of the collection
 */
function revision_collection_delete($cid) {
	db_delete('revision_collections')
			->condition('cid',$cid,'=')
			->execute();
	db_delete('revision_collection_data')
			->condition('cid',$cid,'=')
			->execute();				
}
/**
 * returns a collection
 *
 * @param $cid
 *   The id of the collection
 */
function revision_collections_get_collection($cid) {
	return db_select('revision_collections', 'r')
			->fields('r')
			->condition('r.cid',$cid,'=')
			->execute()
			->fetchObject();			
}
/**
 * returns a collection from a node
 *
 * @param $nid
 *   The id of the collection
 */
function revision_collections_get_collection_from_nid($nid) {
	return db_select('revision_collection_data', 'r')
			->fields('r',array('cid'))
			->condition('r.nid',$nid,'=')
			->execute()
			->fetchObject();			
}
/**
 * returns all collections
 */
function revision_collections_get_collections() {
	$rows = array();
	$results = db_select('revision_collections', 'r')
			->fields('r')
			->execute();
	while ($row = $results->fetchObject()) {
		$rows[] = array(
			'cid' => $row->cid,
		  'title' => $row->title,
			'status' => $row->status,
		);	
	}
	return $rows;
}
/**
 * Add a node to a collection
 *
 * @param $node
 *   node object
 */
function revision_collections_node_insert($node) {
	$types = variable_get('revision_collections_types');
	if($types){
		if($types[$node->type]){
			$cid = revision_collections_get_active_cid();
			db_insert('revision_collection_data')
				->fields(array(
					'nid' => $node->nid,
					'cid' => $cid,
				))	
				->execute();
		}		
	}
}
/**
 * returns the active collection id
 *
 */
function revision_collections_get_active_cid() {
	global $user;	
	if(variable_get('revision_collections_user_level',0) && $user->uid) {
		$results = db_select('revision_collection_user_settings','r')
			->fields('r',array('cid'))
			->condition('uid',$user->uid,'=')
			->execute()
			->fetchObject();	
		return (isset($results->cid) ? $results->cid : -1);
	}
	else return variable_get('revision_collections_global',0);
}
/**
 * Implements of hook_form_alter()
 *
 */
function revision_collections_form_node_form_alter(&$form, &$form_state, $form_id){
	$types = variable_get('revision_collections_types');
	if($types){
		if($types[$form['type']['#value']]){	
			$form['revision_collection'] = array(
				'#type' => 'fieldset',
				'#title' => t('Revision collection settings'),
				'#collapsed' => FALSE,
				'#collapsible' => TRUE,
				'#group' => 'additional_settings',
				'#attributes' => array('class' => array('revision-collection-settings-form')),
			);
			$options = null;
			foreach(revision_collections_get_collections() as $collection) {
				if($collection['status']=='1') {
					$options[$collection['cid']] = $collection['title'];
				}
			}
			$active_cid = revision_collections_get_active_cid();
			if($options) {
				$options[-1] = t('None');
				$form['revision_collection']['revision_collection'] = array(
					'#type' => 'select',
					'#title' => t('Revision collection'),
					'#description' => t('Place node into a collection'),
					'#options' => $options,									
				);				
				if(isset($form['nid']['#value'])){					
					$form['revision_collection']['revision_collection']['#default_value'] = !is_null(revision_collections_get_collection_from_nid($form['nid']['#value'])->cid) ? revision_collections_get_collection_from_nid($form['nid']['#value'])->cid : -1;
				}
				else{
					$form['revision_collection']['revision_collection']['#default_value'] = $active_cid;	
				}
			}
			else {
				$form['revision_collection']['revision_collection_info'] = array(
					'#type' => 'markup',
					'#title' => t('Revision collection'),
					'#markup' => t('You must create a collection first.'),
				);				
			}
			if (user_access('create revision collections')) {
				$form['actions']['submit']['#submit'][] = 'revision_collection_node_form_submit';
			}
		}			
	}
}
/**
 * Implements of hook_form_submit()
 *
 */
function revision_collection_node_form_submit($form, &$form_state){
	if($form_state['values']['revision_collection'] && $form_state['values']['revision_collection']!='-1') {
		db_merge('revision_collection_data')
			->key(array('nid' => $form_state['values']['nid']))
			->fields(array(
				'cid' => $form_state['values']['revision_collection'],
			))	
			->execute();
	}
	else {
		db_merge('revision_collection_data')
			->key(array('nid' => $form_state['values']['nid']))
			->fields(array(
				'cid' => null,
			))			
			->execute();
	}
}
/**
 * Implements of hook_form_alter()
 *
 */
function revision_collections_form_user_profile_form_alter(&$form, &$form_state, $form_id) {
	if(variable_get('revision_collections_user_level',0)){
		foreach(revision_collections_get_collections() as $collection) {
			if($collection['status']=='1') {
			$options[$collection['cid']] = $collection['title'];
			}
		}
		$form['revision_collection'] = array(
			'#type' => 'select',
			'#title' => t('Revision collection'),
			'#description' => t('Your active revision collection'),
			'#options' => $options,
			'#default_value' => revision_collections_get_active_cid(),
		);
		$form['#submit'][] = 'revision_collections_form_user_profile_form_submit';
	}
}
/**
 * Implements of hook_form_submit()
 *
 */
function revision_collections_form_user_profile_form_submit($form, &$form_state) {
	global $user;
	db_merge('revision_collection_user_settings')
		->key(array('uid' => $user->uid))
		->fields(array(
			'uid' => $user->uid,
			'cid' => $form_state['values']['revision_collection'],
		))	
		->execute();	
}
/**
 * Implements hook_node_access()
 *
 */
function revision_collections_node_access($node, $op, $account) {
	if(is_object($node))	{
		$results = db_select('revision_collection_data', 'r')
			->fields('r',array('nid'))
			->condition('cid', revision_collections_get_active_cid(), '=')
			->condition('nid', $node->nid, '=')
			->execute()
			->fetchObject();
			if($results){

				return NODE_ACCESS_ALLOW;
			}
		$results = db_select('revision_collection_data', 'r')
			->fields('r',array('nid'))
			->condition('nid', $node->nid, '=')
			->execute()
			->fetchObject();
			if(!$results){
				return NODE_ACCESS_ALLOW;
			}	
			return NODE_ACCESS_DENY;
	}
}
/**
 * Implements hook_query_alter()
 *
 */
function revision_collections_query_alter(QueryAlterableInterface $query) {
	if($query->hasTag('node_access')) {
			$tables = $query->getTables();
			if(isset($tables['n'])){
				$query->leftJoin('revision_collection_data', 'r', 'n.nid=r.nid');
				$or = db_or()
					->isNull('r.cid')
					->condition('r.cid', revision_collections_get_active_cid(), '=');
				$query->condition($or);
			}			
		}
}
/**
 * Implements hook_views_api()
 *
 */
function revision_collections_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'revision_collections') . '/views',
  );
}