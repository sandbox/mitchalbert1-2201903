<?php
/**
* @file
* Administrative functions for the revision collection module
*/

function revision_collections_settings_form(){
	$form['revision_collection'] = array(
		'#type' => 'fieldset',
		'#title' => t('Revision collection global settings'),
	);
	$form['revision_collection']['revision_collections_user_level'] = array(
		'#type' => 'checkbox',
		'#title' => t('On user level'),
		'#description' => t('Collections can be set on user level.'),
		'#default_value' => variable_get('revision_collections_user_level',0),
	);
	$collection_options = null;
	foreach(revision_collections_get_collections() as $collection){
		if($collection['status']=='1'){
		$collection_options[$collection['cid']] = $collection['title'];
		}
	}
	if($collection_options) {
		$form['revision_collection']['revision_collections_global'] = array(
			'#type' => 'select',
			'#title' => t('Active global collection'),
			'#options' => $collection_options,
			'#description' => t('Collections that can be seen by anonymous or users without a collection'),
			'#default_value' => variable_get('revision_collections_global',0),
		);
	}
	$types = node_type_get_types();
	foreach($types as $type){
		$options[$type->type] = $type->name;
	}
	$form['revision_collection']['revision_collections_types'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Types'),
		'#description' => t('Select the content types that are included in a collection'),
		'#options' => $options,
		'#default_value' => variable_get('revision_collections_types',array()),
	);
	return system_settings_form($form);
}

function revision_collections_overview(){
	$output = l('Add collection','admin/content/revision_collection/add');
	$header = array('cid', t('Title'), t('Status'),t('Operations'));
  $rows = revision_collections_get_collections();
	foreach($rows as $key => $row){
		$rows[$key]['status'] = ($row['status'] ? t('Published') : t('not published'));
		$rows[$key]['moderate'] = l(t('edit'),'content/revision_collection/'.$row['cid'].'/edit')
										.' '.l(t('delete'),'content/revision_collection/'.$row['cid'].'/delete');
	}	
	$output.= theme('table', array('header' => $header, 'rows' => $rows));
	return $output;
}

function revision_collections_edit($cid){
	return drupal_get_form('revision_collections_add_form',$cid);
}

function revision_collections_add(){
	return drupal_get_form('revision_collections_add_form');
}

function revision_collections_add_form(){
$args = func_get_args();
	$collection = null;
	if(isset($args[1]['build_info']['args'][0])){
		$collection = revision_collections_get_collection($args[1]['build_info']['args'][0]);
	}
	$form['title'] = array(
		'#type' => 'textfield',
		'#required' => TRUE,
		'#title' => t('Title'),
		'#default_value' => $collection ? $collection->title : '',		
	);
	$form['published'] = array(
		'#type' => 'radios',
		'#options' => array(1 => 'Yes', 0 => 'No'),
		'#title' => t('Published'),
		'#default_value' => $collection ? $collection->status : 1,		 
	);
	if($collection){
		$form['cid'] = array(
			'#type' => 'hidden',
			'#default_value' => $collection->cid,			
		);		
	}
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		
	);
	return $form;
}

function revision_collections_add_form_submit($form, &$form_state){
	$query = db_merge('revision_collections')
		->key(array('cid' => $form_state['values']['cid']))
		->fields(array(
			'title' => $form_state['values']['title'],
			'status' => $form_state['values']['published'],
		));
		$query->execute();

		drupal_goto('admin/content/revision_collection');
}

function revision_collection_delete_confirm($form, &$form_state, $cid){

	$collection = revision_collections_get_collection($cid);
	$form['#collection'] = $collection;
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $collection->title)),
    'admin/content/revision_collection',
    t('All content within the collection will have no collection. This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );	
}

function revision_collection_delete_confirm_submit($form, &$form_state){
  if ($form_state['values']['confirm']) {
    revision_collection_delete($form_state['complete form']['#collection']->cid);
    watchdog('content', 'Collection: deleted %title.', array('%title' => $form_state['complete form']['#collection']->title));
    drupal_set_message(t('Collection %title has been deleted.', array('%title' => $form_state['complete form']['#collection']->title)));
  }
  $form_state['redirect'] = '<front>';	
}