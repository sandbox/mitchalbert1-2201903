<?php
/**
 * Implements hook_views_data()
 *
 */
function revision_collections_views_data() {
	$data = array();
	$data['revision_collections']['table']['group'] = t('Revision collections');
	$data['revision_collection_data']['table']['group'] = t('Revision collections data');

	$data['revision_collections']['table']['join']['node'] = array(
		'left_table' => 'revision_collection_data',
		'left_field' => 'cid',
		'field' => 'cid',
	);
	$data['revision_collection_data']['table']['join']['node'] = array(
		'left_field' => 'nid',
		'field' => 'nid',
	);
	$data['revision_collections']['ciid'] = array(
		'title' => t('Ciid'),
		'help' => t('The collection ID of the revision collection.'), 
		'field' => array(
			'handler' => 'views_handler_field_node',
			'click sortable' => TRUE,
		),
		'argument' => array(
			'handler' => 'views_handler_argument_node_nid',
			'name field' => 'cid', 
			'numeric' => TRUE,
			'validate type' => 'nid',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_numeric',
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
    'relationship' => array(
      'label' => t('Cid'),
      'base' => 'revision_collection_data',
      'base field' => 'cid',
    ),
	);	
	$data['revision_collections']['cid'] = array(
		'title' => t('Cid'),
		'help' => t('The collection ID'), 
		'field' => array(
			'handler' => 'views_handler_field_node',
			'click sortable' => TRUE,
		),
		'argument' => array(
			'handler' => 'views_handler_argument_node_nid',
			'name field' => 'cid', 
			'numeric' => TRUE,
			'validate type' => 'nid',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_numeric',
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
    'relationship' => array(
      'label' => t('Cid'),
      'base' => 'revision_collection_data',
      'base field' => 'cid',
    ),
	);
	$data['revision_collections']['title'] = array(
		'title' => t('Title'),
		'help' => t('The title of the collection'), 
		'field' => array(
			'handler' => 'views_handler_field_node',
			'click sortable' => TRUE,
		),		
		'argument' => array(
			'handler' => 'views_handler_argument_node_nid',
			'name field' => 'title', 
			'numeric' => TRUE,
			'validate type' => 'nid',
		),		
		'filter' => array(
			'handler' => 'views_handler_filter_numeric',
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
	);
	$data['revision_collection_data']['nid'] = array(
		'title' => t('Nid'),
		'help' => t('The id of the collected node'), 
		'field' => array(
			'handler' => 'views_handler_field_node',
			'click sortable' => TRUE,
		),
		'argument' => array(
			'handler' => 'views_handler_argument_node_nid',
			'name field' => 'nid',
			'numeric' => TRUE,
			'validate type' => 'nid',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_numeric',
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
    'relationship' => array(
      'label' => t('The id of the collected node'),
      'base' => 'node',
      'base field' => 'nid',
    ),
	);
  $data['revision_collections']['cid_current'] = array(
    'real field' => 'cid',
    'title' => t('Current'),
    'help' => t('Filter the view to the active collection.'),
    'filter' => array(
      'handler' => 'views_handler_filter_revision_collections_current',
      'type' => 'yes-no',
    ),
  );
  return $data;
}